'use strict';

const tabList = document.querySelector('.tabs');
const tabMenu = document.querySelector('.tabs-content');
const tabsContentItems = [...tabMenu.children];

tabList.addEventListener('click', e => {
    const target = e.target;

    if (target !== tabList) {
        [...tabList.children].forEach(item => {
            item.classList.remove('active');
        });

        tabsContentItems.forEach(tab => {
            if (tab.dataset.tabs === target.dataset.tabs) {
                tab.classList.add('active');
            } else {
                tab.classList.remove('active');
            }
        });

        target.classList.toggle('active');
    }
    
    tabsContentItems.forEach(tab => {
        if (tab.dataset.tabs === target.dataset.tabs) {
            tab.classList.add('active');
            tab.classList.remove('hidden');
        } else {
            tab.classList.add('hidden');
        }
    });
});

